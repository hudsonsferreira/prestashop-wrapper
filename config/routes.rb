Rails.application.routes.draw do

  resources :sessions, only: [:new, :create]
  resources :products, only: [:new, :create, :index, :show]

  root to: 'sessions#new'
end
