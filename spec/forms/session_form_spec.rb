require 'rails_helper'

RSpec.describe SessionForm, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :presta_shop_url }
    it { is_expected.to validate_presence_of :presta_shop_api_key }
  end
end
