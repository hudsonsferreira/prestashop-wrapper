require 'rails_helper'

RSpec.describe ProductForm, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :reference }
    it { is_expected.to validate_presence_of :price }
  end

  describe '#save' do
    before do
      allow(PrestashopProductCreator).to receive(:call)
    end

    context 'when valid' do
      it 'calls PrestashopProductCreator.call service' do
        product = described_class.new({ name: 'ipod', reference: 'ipod', price: 200 })

        product.save

        expect(PrestashopProductCreator).to have_received(:call)
      end
    end

    context 'when invalid' do
      it 'does not call PrestashopProductCreator.call service' do
        product = described_class.new

        product.save

        expect(PrestashopProductCreator).not_to have_received(:call)
      end
    end
  end
end
