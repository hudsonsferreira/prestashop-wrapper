require 'rails_helper'

RSpec.describe PrestashopProductCreator do
  describe '.call' do
    context 'when valid' do
      let(:product_mapper) { instance_double Prestashop::Mapper::Product }

      let(:params) do
        {
          id_lang: 1,
          id_supplier: 1,
          available_now: 'yes',
          available_later: 'yes',
          name: 'macbook',
          reference: 'macbook-pro',
          price: 2300.5
        }
      end

      before do
        allow(PrestashopConnection).to receive(:connection)
          .with('test.com', 'bar123')

        allow(Prestashop::Mapper::Product).to receive(:new)
          .with(params)
          .and_return(product_mapper)

        allow(product_mapper).to receive(:create)
      end

      it 'calls PrestashopConnection.connection' do
        expect(PrestashopConnection).to receive(:connection)
          .with('test.com', 'bar123')

        described_class.call(url: 'test.com', api_key: 'bar123', params: params)
      end

      it 'calls Prestashop::Mapper::Product#create' do
        expect(Prestashop::Mapper::Product).to receive(:new)
          .with(params)
          .and_return(product_mapper)

        expect(product_mapper).to receive(:create)

        described_class.call(url: 'test.com', api_key: 'bar123', params: params)
      end
    end

    context 'when invalid' do
      let(:params) do
        {
          name: 'macbook',
          reference: 'macbook-pro',
          price: 2300.5
        }
      end

      before do
        allow(PrestashopConnection).to receive(:connection)
          .with('test.com', 'bar123')

        allow(Prestashop::Mapper::Product).to receive(:new)
          .and_raise(Prestashop::Api::ParserError)
      end

      it 'returns true when raises an error' do
        response = described_class.call(url: 'test.com', api_key: 'bar123', params: params)

        expect(response).to be_truthy
      end
    end
  end
end
