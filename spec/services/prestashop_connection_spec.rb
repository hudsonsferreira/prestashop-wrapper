require 'rails_helper'

RSpec.describe PrestashopConnection do
  describe '.connection' do
    let(:implementation) { instance_double Prestashop::Client::Implementation }
    let(:connection) { instance_double Prestashop::Api::Connection }

    before do
      allow(Prestashop::Client::Implementation).to receive(:create)
        .with('bar123', 'test.com')
        .and_return(implementation)

      allow(implementation).to receive(:connection).and_return(connection)
    end

    it 'returns Prestashop connection instance' do
      expect(Prestashop::Client::Implementation).to receive(:create)
        .with('bar123', 'test.com')
        .and_return(implementation)

      expect(implementation).to receive(:connection).and_return(connection)

      described_class.connection('test.com', 'bar123')
    end
  end
end
