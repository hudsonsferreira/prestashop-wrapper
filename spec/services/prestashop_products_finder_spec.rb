require 'rails_helper'

RSpec.describe PrestashopProductsFinder do
  describe '.call' do
    context 'when looking for a product list' do
      let(:ws_response) do
        {
          :products => {
            :product => [
              { :attr => { :id => 1 } }, { :attr => { :id => 2 } }
            ]
          }
        }
      end

      let(:expected_response) do
        [{ :id => 1 }, { :id => 2 }]
      end

      let(:connection) { instance_double Prestashop::Api::Connection }

      before do
        allow(PrestashopConnection).to receive(:connection)
          .with('test.com', 'bar123')
          .and_return(connection)

        allow(connection).to receive(:read).with(:products).and_return(ws_response)
      end

      it 'calls PrestashopConnection.connection' do
        expect(PrestashopConnection).to receive(:connection)
          .with('test.com', 'bar123')
          .and_return(connection)

        expect(connection).to receive(:read).with(:products).and_return(ws_response)

        described_class.call(url: 'test.com', api_key: 'bar123')
      end

      it 'returns prestashop products' do
        result = described_class.call(url: 'test.com', api_key: 'bar123')

        expect(result).to eq(expected_response)
      end
    end

    context 'when looking for product detail' do
      let(:ws_response) do
        { :product =>
          { :id => 1,
            :minimal_quantity => 1,
            :price => "16.510000",
            :name => {:language => [{:attr => {:id => 1}, :val => "Faded Short Sleeves T-shirt"}]}
          }
        }
      end

      let(:expected_response) do
        { :id => 1,
          :minimal_quantity => 1,
          :price => "16.510000",
          :name => 'Faded Short Sleeves T-shirt'
        }
      end

      let(:connection) { instance_double Prestashop::Api::Connection }

      context 'when product_id is string' do
        before do
          allow(PrestashopConnection).to receive(:connection)
            .with('test.com', 'bar123')
            .and_return(connection)

          allow(connection).to receive(:read).with(:products, 1).and_return(ws_response)
        end

        it 'calls PrestashopConnection.connection' do
          expect(PrestashopConnection).to receive(:connection)
            .with('test.com', 'bar123')
            .and_return(connection)

          expect(connection).to receive(:read).with(:products, 1).and_return(ws_response)

          described_class.call(url: 'test.com', api_key: 'bar123', product_id: '1')
        end

        it 'returns prestashop product detail' do
          result = described_class.call(url: 'test.com', api_key: 'bar123', product_id: '1')

          expect(result).to eq(expected_response)
        end
      end

      context 'when product_id is integer' do
        before do
          allow(PrestashopConnection).to receive(:connection)
            .with('test.com', 'bar123')
            .and_return(connection)

          allow(connection).to receive(:read).with(:products, 1).and_return(ws_response)
        end

        it 'calls PrestashopConnection.connection' do
          expect(PrestashopConnection).to receive(:connection)
            .with('test.com', 'bar123')
            .and_return(connection)

          expect(connection).to receive(:read).with(:products, 1).and_return(ws_response)

          described_class.call(url: 'test.com', api_key: 'bar123', product_id: 1)
        end

        it 'returns prestashop product detail' do
          result = described_class.call(url: 'test.com', api_key: 'bar123', product_id: 1)

          expect(result).to eq(expected_response)
        end
      end
    end
  end
end
