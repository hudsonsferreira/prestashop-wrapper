require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  describe 'GET #new' do
    before do
      get :new
    end

    it 'responds successfully with 200' do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it 'assigns SessionForm instance' do
      expect(assigns[:session]).to be_instance_of(SessionForm)
    end

    it 'returns new template' do
      expect(response).to render_template(:new)
    end
  end

  describe 'POST #create' do
    context 'with prestashop config arguments' do
      before do
        post :create, params: { session_form: { presta_shop_url: 'test.com', presta_shop_api_key: 'akas212' } }
      end

      it 'redirects to products_path' do
        expect(response).to redirect_to(products_path)
      end

      it 'assigns session attrs' do
        expect(request.session['presta_shop_url']).to eq('test.com')
        expect(request.session['presta_shop_api_key']).to eq('akas212')
      end
    end

    context 'without prestashop config arguments' do
      it 'renders new template' do
        post :create, params: { session_form: { presta_shop_url: nil, presta_shop_api_key: nil } }

        expect(response).to render_template(:new)
      end
    end
  end
end
