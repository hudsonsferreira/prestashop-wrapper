require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  describe 'GET #new' do
    before do
      session[:presta_shop_url] = 'foo.com'
      session[:presta_shop_api_key] = 'bar123'
    end

    it 'responds successfully with 200' do
      get :new

      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it 'returns new template' do
      get :new

      expect(response).to render_template(:new)
    end

    it 'assigns @product' do
      get :new

      expect(assigns[:product]).to be_instance_of(ProductForm)
    end
  end

  describe 'POST #create' do
    before do
      session[:presta_shop_url] = 'foo.com'
      session[:presta_shop_api_key] = 'bar123'
    end

    context 'with valid params' do
      before do
        allow(PrestashopProductCreator).to receive(:call).and_return(true)
      end

      let(:params) do
        {
          product_form: {
            name: 'imac',
            reference: 'imac-apple',
            price: '2700'
          }
        }
      end

      it 'sets flash message' do
        post :create, params: params

        expect(flash[:notice]).to eq('Product imac created successfully')
      end

      it 'redirects to products_path' do
        post :create, params: params

        expect(response).to redirect_to(products_path)
      end
    end

    context 'with invalid params' do
      it 'renders template new' do
        post :create, params: { product_form: { name: nil } }

        expect(response).to render_template(:new)
      end
    end
  end

  describe 'GET #index' do
    before do
      session[:presta_shop_url] = 'foo.com'
      session[:presta_shop_api_key] = 'bar123'
    end

    let(:expected_products_list) do
      [{ :id => 1 }, { :id => 2 }]
    end

    before do
      request.session['presta_shop_url'] = 'test.com'
      request.session['presta_shop_api_key'] = 'akas212'

      allow(PrestashopProductsFinder).to receive(:call)
        .with(url: 'test.com', api_key: 'akas212')
        .and_return(expected_products_list)
    end

    it 'responds successfully with 200' do
      get :index

      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it 'returns index template' do
      get :index

      expect(response).to render_template(:index)
    end

    it 'calls PrestashopProductsFinder.call service' do
      expect(PrestashopProductsFinder).to receive(:call)
        .with(url: 'test.com', api_key: 'akas212')
        .and_return(expected_products_list)

      get :index
    end

    it 'assigns @products' do
      get :index

      expect(assigns[:products]).to eq expected_products_list
    end
  end

  describe 'GET #show' do
    before do
      session[:presta_shop_url] = 'foo.com'
      session[:presta_shop_api_key] = 'bar123'
    end

    let(:expected_product_detail) do
      { :id => 1,
        :minimal_quantity => 1,
        :price => "16.510000",
        :name => 'Faded Short Sleeves T-shirt'
      }
    end

    before do
      request.session['presta_shop_url'] = 'test.com'
      request.session['presta_shop_api_key'] = 'akas212'

      allow(PrestashopProductsFinder).to receive(:call)
        .with(url: 'test.com', api_key: 'akas212', product_id: "1")
        .and_return(expected_product_detail)
    end

    it 'responds successfully with 200' do
      get :show, params: { id: 1 }

      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it 'returns show template' do
      get :show, params: { id: 1 }

      expect(response).to render_template(:show)
    end

    it 'calls PrestashopProductsFinder.call service' do
      expect(PrestashopProductsFinder).to receive(:call)
        .with(url: 'test.com', api_key: 'akas212', product_id: "1")
        .and_return(expected_product_detail)

      get :show, params: { id: 1 }
    end

    it 'assigns @product' do
      get :show, params: { id: 1 }

      expect(assigns[:product]).to eq expected_product_detail
    end
  end
end
