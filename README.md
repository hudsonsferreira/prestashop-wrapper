# prestashop-wrapper

[![CircleCI](https://circleci.com/gh/Helabs/stone-am/tree/master.svg?style=svg&circle-token=769e07b58fb995dff263ef086c9955f53d6048a0)](https://circleci.com/bb/hudsonsferreira/prestashop-wrapper/tree/master)

## Link to app hosted on Heroku

Open [http://prestashop-wrapper.herokuapp.com](http://prestashop-wrapper.herokuapp.com)

## Dependencies

* Ruby 2.4.0
* PostgreSQL

## Setup

```
1. Install the dependencies above
2. $ git clone <REPOSITORY_URL>
3. $ bin/setup
```

## Running the project

1. `$ bin/rails s`
2. Open [http://localhost:3000](http://localhost:3000)

## Running specs and checking coverage

```
$ coverage=on bin/rspec
```

## Deploying to production

```
$ git push origin master
```

Just access [CircleCI](https://circleci.com/bb/hudsonsferreira/prestashop-wrapper/tree/master) to watch the build :)

## Post mortem

I choose to use [https://github.com/werein/prestashop](https://github.com/werein/prestashop) gem, but I realized that it wasn't the best way when I had to create a product on my store.

Why did I choose it?

1. Documentation was clear enough on how it'll turn easier my journey through REST requests.
2. It was installed more than thousands times via Rubygems.
3. Build is passing and coverage above 90%.

I faced the following scenario:

1. I had to set some default values on create a product through prestashop gem.
2. The request always raises an exception even creating a product on my store. In this case, I'm not sure if I did something wrong configuring my prestashop server locally.

## Technical decisions

1. As there is no requirement to persist data on database, I decided to persist prestashop config on session vars.
2. Form objects were created to assume the responsibility of validates my form values.
3. PrestashopConnection is responsible to connect our app via prestashop gem and return a connection object which is being reused through our another services.
4. PrestashopProductsFinder is responsible to find the requested product list to ProductsController#index and requested product detail to ProductsController#show action.
5. PrestashopProductCreator is responsible to create a new product on the store. As I said I had to mock exception error on this service to make it works, since I'm sure that my products list are being updated.
5. Keep Gemfile slim.


## Disclaimer

1. As I had a deadline I just kept prestashop gem because I believe it does not disturb my app implementation to be evaluated.
2. I didn't have the opportunity to test my app on production because I don't have a prestashop in production to test.

## Suggested next steps

1. Drop prestashop gem and use a most robust solution like Faraday, for example.
2. Add style to interface.
