class ProductsController < ApplicationController
  before_action :ensure_configuration_is_set
  
  def new
    @product = ProductForm.new
  end

  def create
    @product = ProductForm.new(
      product_params.merge(
        url: session[:presta_shop_url],
        api_key: session[:presta_shop_api_key]
      )
    )

    if @product.save
      redirect_to products_path, notice: "Product #{@product.name} created successfully"
    else
      render :new
    end
  end

  def index
    @products = PrestashopProductsFinder.call(
      url: session[:presta_shop_url],
      api_key: session[:presta_shop_api_key]
    )
  end

  def show
    @product = PrestashopProductsFinder.call(
      url: session[:presta_shop_url],
      api_key: session[:presta_shop_api_key],
      product_id: params[:id]
    )
  end

  private

  def product_params
    params.require(:product_form).permit(:name, :reference, :price)
  end
end
