class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def ensure_configuration_is_set
    if session[:presta_shop_url].nil? || session[:presta_shop_api_key].nil?
      redirect_to new_session_path
    end
  end
end
