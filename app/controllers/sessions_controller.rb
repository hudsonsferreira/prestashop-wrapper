class SessionsController < ApplicationController
  def new
    @session = SessionForm.new(
      presta_shop_url: session[:presta_shop_url],
      presta_shop_api_key: session[:presta_shop_api_key]
    )
  end

  def create
    @session = SessionForm.new(session_params)

    if @session.valid?
      assign_session_attrs
      redirect_to products_path
    else
      render :new
    end
  end

  private

  def session_params
    params.require(:session_form).permit(:presta_shop_url, :presta_shop_api_key)
  end

  def assign_session_attrs
    session[:presta_shop_url] = @session.presta_shop_url
    session[:presta_shop_api_key] = @session.presta_shop_api_key
  end
end
