class PrestashopProductsFinder
  def self.call(url:, api_key:, product_id: nil)
    new(url, api_key, product_id).call
  end

  def initialize(url, api_key, product_id)
    @connection = PrestashopConnection.connection(url, api_key)
    @product_id = product_id.to_i if product_id
  end

  def call
    if @product_id.present?
      product_detail
    else
      products_list
    end
  end

  private

  def products_list
    raw_products_list = @connection.read(:products)
    raw_products_list[:products][:product].map! { |product| product[:attr] }
  end

  def product_detail
    raw_product_detail = @connection.read(:products, @product_id)

    {
      id:                raw_product_detail[:product][:id],
      minimal_quantity:  raw_product_detail[:product][:minimal_quantity],
      price:             raw_product_detail[:product][:price],
      name:              raw_product_detail[:product][:name][:language].first[:val]
    }
  end
end
