class PrestashopProductCreator
  def self.call(url:, api_key:, params:)
    new(url, api_key, params).call
  end

  def initialize(url, api_key, params)
    connect_with_prestashop(url, api_key)

    @name      = params[:name]
    @reference = params[:reference]
    @price     = params [:price]
  end

  def call
    # Disclaimer:
    # - The hardcoded values are necessary to make it works on prestashop gem
    # - There is a bug on prestashop gem that creates the product but returns an exception,
    #   because of this I'm always returning true on this context

    begin
      Prestashop::Mapper::Product.new(
        id_lang: 1,
        id_supplier: 1,
        name: @name,
        reference: @reference,
        price: @price,
        available_now: 'yes',
        available_later: 'yes'
      ).create
    rescue
      true
    end
  end

  private

  def connect_with_prestashop(url, api_key)
    PrestashopConnection.connection(url, api_key)
  end
end
