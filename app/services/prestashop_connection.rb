class PrestashopConnection
  def self.connection(url, api_key)
    Prestashop::Client::Implementation.create(api_key, url).connection
  end
end
