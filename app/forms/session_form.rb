class SessionForm
  include ActiveModel::Model

  validates :presta_shop_url, :presta_shop_api_key, presence: true
  attr_accessor :presta_shop_url, :presta_shop_api_key
end
