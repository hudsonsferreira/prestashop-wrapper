class ProductForm
  include ActiveModel::Model

  attr_accessor :name, :reference, :price, :url, :api_key
  validates :name, :reference, :price, presence: true

  def save
    create_product_on_prestashop if valid?
  end

  private

  def create_product_on_prestashop
    PrestashopProductCreator.call(
      url: url,
      api_key: api_key,
      params: { name: name, reference: reference, price: price }
    )
  end
end
